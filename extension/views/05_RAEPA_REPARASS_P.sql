CREATE OR REPLACE VIEW qgep_raepa.raepa_reparass AS
SELECT
  mevent.obj_id AS idrepar,
  'TBD' AS x,
  'TBD' AS y,
  'TBD' AS supprepar,
  '00' AS defreparee,
  'TBD' AS idsuprepar,
  mevent.time_point AS daterepar,
  org.identifier AS mouvrage
FROM qgep_od.vw_qgep_maintenance as mevent
LEFT JOIN qgep_od.organisation as org
ON mevent.fk_operating_company = org.obj_id
WHERE mevent.kind = '4528'
