CREATE OR REPLACE VIEW qgep_raepa.raepa_canalass_l AS
SELECT
  reach.obj_id::text AS idcana ,
  reach.progression_geometry AS geom,
  'TBD' as enservice,
  raepa_branchement.raepa_code AS branchement,
  raepa_typreseau.raepa_code as typreseau,
  raepa_contcanass.raepa_code as contcanass,
  raepa_fonccanass.raepa_code as fonccanass,
  raepa_materiau.raepa_code AS materiau,
  reach.clear_height AS diametre,
  reach.ws_year_of_construction AS anfinose,
  raepa_modecirc.raepa_code AS modecirc,
  reach.rp_from_obj_id as idnini,
  reach.rp_to_obj_id as idnterm,
  'TBD' as idcanppale,
  'TBD' as andebpose,
  reach.length_effective as longcana,
  'TBD' as nbranche,
  'TBD' as qualglocxy,
  'TBD' as qualglocz,
  'TBD' as datemaj,
  'TBD' as sourmaj,
  'TBD' as qualannee,
  'TBD' as dategeoloc,
  'TBD' as sourgeoloc,
  'TBD' as sourattrib
FROM
    qgep_od.vw_qgep_reach as reach
    LEFT JOIN qgep_raepa.qgep_raepa_canalass_branchement as raepa_branchement
	ON reach.ch_function_hierarchic = raepa_branchement.code
	LEFT JOIN qgep_raepa.qgep_raepa_canalass_typreseau as raepa_typreseau
	ON reach.ch_usage_current = raepa_typreseau.code
	LEFT JOIN qgep_raepa.qgep_raepa_canalass_contcanass as raepa_contcanass
	ON reach.ch_usage_current = raepa_contcanass.code
	LEFT JOIN qgep_raepa.qgep_raepa_canalass_fonccanass as raepa_fonccanass
	ON reach.ch_function_hydraulic = raepa_fonccanass.code
	LEFT JOIN qgep_raepa.qgep_raepa_canalass_materiau as raepa_materiau
	ON reach.material = raepa_materiau.code
	LEFT JOIN qgep_raepa.qgep_raepa_canalass_modecirc as raepa_modecirc
	ON reach.ch_function_hydraulic = raepa_modecirc.code
