CREATE OR REPLACE VIEW qgep_raepa.raepa_ouvrass_l AS
SELECT
  struct.obj_id AS idouvrage,
  struct.situation_geometry AS geom,
  ST_X (ST_Transform (struct.situation_geometry, 2154))::Numeric(10 , 3) AS x,
  ST_Y (ST_Transform (struct.situation_geometry, 2154))::Numeric(10 , 3) AS y,
  'TBD' AS mouvrage,
  'TBD' AS gexploit,
  raepa_typreseau.raepa_code AS typreseau,
  CASE
      WHEN (struct.ws_type = 'manhole') THEN raepa_fnouvass_ma.raepa_code 
      WHEN (struct.ws_type = 'special_structure') THEN raepa_fnouvass_ss.raepa_code 
      ELSE 'unknown'::text
  END AS fnouvass,
  struct.year_of_construction AS anfinpose,
  'TBD' AS idcanamont,
  'TBD' AS idcanaval,
  'TBD' AS idcanppale,
  ST_Z(struct.situation_geometry) AS z,
  struct.year_of_construction AS andebpose,
  raepa_position_precision.raepa_code AS qualglocxy,
  raepa_position_precision.raepa_code AS qualglocz,
  struct.last_modification AS datemaj,
  'TBD' AS sourmaj,
  '00' AS qualannee,
  'TBD' AS dategeoloc,
  'TBD' AS sourgeoloc,
  'TBD' AS sourattrib  
FROM
    qgep_od.vw_qgep_wastewater_structure as struct
    LEFT JOIN qgep_raepa.qgep_raepa_canalass_typreseau as raepa_typreseau
	ON struct._channel_usage_current = raepa_typreseau.code
	LEFT JOIN qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma as raepa_fnouvass_ma
	ON struct.ma_function = raepa_fnouvass_ma.code
	LEFT JOIN qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss as raepa_fnouvass_ss
	ON struct.ss_function = raepa_fnouvass_ss.code
	LEFT JOIN qgep_raepa.qgep_raepa_ouvrass_position_precision as raepa_position_precision
	ON struct.co_positional_accuracy = raepa_position_precision.code
