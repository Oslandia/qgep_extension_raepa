CREATE OR REPLACE VIEW qgep_raepa.raepa_apparass AS
SELECT
  struct.obj_id AS idappareil,
  ST_X (ST_Transform (mpoint.situation_geometry, 2154))::Numeric(10 , 3) AS x,
  ST_Y (ST_Transform (mpoint.situation_geometry, 2154))::Numeric(10 , 3) AS y,
  'TBD' AS mouvrage,
  'TBD' AS gexploit,
  raepa_typreseau.raepa_code AS typreseau,
  '00' AS fnappass,
  struct.year_of_construction AS anfinpose,
  'TBD' AS diametre,
  'TBD' AS idcanamont,
  'TBD' AS idcanaval,
  'TBD' AS idcanppale,
  struct.obj_id AS idouvrage,
  ST_Z(struct.situation_geometry) AS z,
  struct.year_of_construction AS andebpose,
  raepa_position_precision.raepa_code AS qualglocxy,
  raepa_position_precision.raepa_code AS qualglocz,
  struct.last_modification AS datemaj,
  'TBD' AS sourmaj,
  'TBD' AS qualannee,
  'TBD' AS dategeoloc,
  'TBD' AS sourgeoloc,
  'TBD' AS sourattrib  
FROM
    qgep_od.measuring_device as device
    LEFT JOIN qgep_od.measuring_point as mpoint
    ON device.fk_measuring_point = mpoint.obj_id
    LEFT JOIN qgep_od.vw_qgep_wastewater_structure as struct
    ON mpoint.fk_wastewater_structure = struct.obj_id
    LEFT JOIN qgep_raepa.qgep_raepa_canalass_typreseau as raepa_typreseau
	ON struct._channel_usage_current = raepa_typreseau.code
    LEFT JOIN qgep_raepa.qgep_raepa_ouvrass_position_precision as raepa_position_precision
	ON struct.co_positional_accuracy = raepa_position_precision.code
