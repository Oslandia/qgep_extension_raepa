-- Table: qgep_raepa.qgep_raepa_canalass_branchement

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_branchement;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_branchement
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_branchement_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5062, 'OAP.conduite_d_assainissement', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5063, 'OAS.conduite_d_assainissement', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5064, 'OAP.evacuation_bien_fonds', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5065, 'OAS.evacuation_bien_fonds', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5066, 'OAP.autre', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5067, 'OAS.autre', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5068, 'OAP.cours_d_eau', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5069, 'OAP.collecteur_principal', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5070, 'OAP.collecteur_principal_regional', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5071, 'OAP.collecteur', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5072, 'OAP.evacuation_des_eaux_de_routes', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5073, 'OAS.evacuation_des_eaux_de_routes', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5074, 'OAP.inconnue', 'N');
INSERT INTO qgep_raepa.qgep_raepa_canalass_branchement VALUES (5075, 'OAS.inconnue', 'N');

-- Table: qgep_raepa.qgep_raepa_canalass_contcanass

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_contcanass;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_contcanass
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_contcanass_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4514, 'eaux_claires', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4516, 'eaux_mixtes_deversees', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4518, 'eaux_cours_d_eau', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4520, 'eaux_pluviales', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4522, 'eaux_mixtes', '03');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4524, 'eaux_industrielles', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4526, 'eaux_usees', '02');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (4571, 'inconnu', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_contcanass VALUES (5322, 'autre', '99');
    
-- Table: qgep_raepa.qgep_raepa_canalass_fonccanass

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_fonccanass;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_fonccanass
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_fonccanass_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5062, 'OAP.conduite_d_assainissement', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5063, 'OAS.conduite_d_assainissement', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5064, 'OAP.evacuation_bien_fonds', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5065, 'OAS.evacuation_bien_fonds', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5066, 'OAP.autre', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5067, 'OAS.autre', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5068, 'OAP.cours_d_eau', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5069, 'OAP.collecteur_principal', '02');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5070, 'OAP.collecteur_principal_regional', '02');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5071, 'OAP.collecteur', '02');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5072, 'OAP.evacuation_des_eaux_de_routes', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5073, 'OAS.evacuation_des_eaux_de_routes', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5074, 'OAP.inconnue', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_fonccanass VALUES (5075, 'OAS.inconnue', '00');
    
-- Table: qgep_raepa.qgep_raepa_canalass_materiau

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_materiau;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_materiau
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_materiau_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (2754, 'amiante_ciment', '02');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3638, 'beton_normal', '06');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3639, 'beton_coule_sur_place', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3640, 'beton_pousse_tube', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3641, 'beton_special', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3256, 'beton_inconnu', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (147, 'fibrociment', '10');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (2755, 'terre_cuite', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (148, 'fonte_ductile', '11');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3648, 'fonte_grise', '12');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5076, 'matiere_synthetique_resine_d_epoxy', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5077, 'matiere_synthetique_polyethylene_dur', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5078, 'matiere_synthetique_polyester_GUP', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5079, 'matiere_synthetique_polyethylene', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5080, 'matiere_synthetique_polypropylene', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5081, 'matiere_synthetique_PVC', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5382, 'matiere_synthetique_inconnue', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (153, 'acier', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3654, 'acier_inoxydable', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (154, 'gres', '13');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (2761, 'argile', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (3016, 'inconnu', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (2762, 'ciment', '08');
INSERT INTO qgep_raepa.qgep_raepa_canalass_materiau VALUES (5381, 'autre', '99');
    
-- Table: qgep_raepa.qgep_raepa_canalass_modecirc

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_modecirc;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_modecirc
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_modecirc_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (5320, 'autre', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (2546, 'conduite_de_transport_pour_le_drainage', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (22, 'conduite_d_etranglement', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (3610, 'siphon_inverse', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (367, 'conduite_a_ecoulement_gravitaire', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (23, 'conduite_de_refoulement', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (145, 'conduite_de_drainage', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (21, 'conduite_de_retention', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (144, 'conduite_de_rincage', '99');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (5321, 'inconnue', '00');
INSERT INTO qgep_raepa.qgep_raepa_canalass_modecirc VALUES (3655, 'conduite_sous_vide', '03');
    
-- Table: qgep_raepa.qgep_raepa_canalass_typreseau

-- DROP TABLE qgep_raepa.qgep_raepa_canalass_typreseau;

CREATE TABLE qgep_raepa.qgep_raepa_canalass_typreseau
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_canalass_typreseau_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4514, 'eaux_claires', NULL);
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4518, 'eaux_cours_d_eau', NULL);
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4524, 'eaux_industrielles', NULL);
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4571, 'inconnu', NULL);
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (5322, 'autre', NULL);
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4516, 'eaux_mixtes_deversees', '03');
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4520, 'eaux_pluviales', '01');
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4522, 'eaux_mixtes', '03');
INSERT INTO qgep_raepa.qgep_raepa_canalass_typreseau VALUES (4526, 'eaux_usees', '02');
    
-- Table: qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma

-- DROP TABLE qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma;

CREATE TABLE qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_ouvrass_fnouvass_ma_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (4532, 'ouvrage_de_chute', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (5344, 'autre', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (4533, 'aeration', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (3267, 'chambre_recolte_eaux_toitures', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (3266, 'chambre_avec_grille_d_entree', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (3472, 'rigole_de_drainage', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (228, 'evacuation_des_eaux_des_voies_ferrees', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (204, 'regard_de_visite', '06');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (1008, 'separateur_d_hydrocarbures', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (4536, 'station_de_pompage', '01');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (5346, 'deversoir_d_orage', '04');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (2742, 'depotoir', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (5347, 'separateur_de_materiaux_flottants', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (4537, 'chambre_de_chasse', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (4798, 'ouvrage_de_repartition', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ma VALUES (5345, 'inconnue', '00');
    
-- Table: qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss

-- DROP TABLE qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss;

CREATE TABLE qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_ouvrass_fnouvass_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (6397, 'fosse_etanche', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (245, 'ouvrage_de_chute', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (6398, 'fosse_digestive', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5371, 'autre', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (386, 'aeration', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3234, 'chambre_avec_siphon_inverse', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5091, 'entree_de_siphon', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (6399, 'fosse_septique', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3348, 'depression_de_terrain', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (336, 'depotoir_pour_alluvions', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5494, 'fosse_a_purin', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (6478, 'fosse_de_decantation', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (2998, 'regard_de_visite', '06');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (2768, 'separateur_d_hydrocarbures', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (246, 'station_de_pompage', '01');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3673, 'BEP_decantation', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3674, 'BEP_retention', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5574, 'BEP_canal_retention', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3675, 'BEP_clarification', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3676, 'BEP_accumulation', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5575, 'BEP_canal_accumulation', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5576, 'BEP_canal_stockage', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3677, 'BEP_combine', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5372, 'deversoir_d_orage', '04');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (5373, 'separateur_de_materiaux_flottants', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (383, 'acces_lateral', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (227, 'chambre_de_chasse', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (4799, 'ouvrage_de_repartition', '99');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (3008, 'inconnu', '00');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_fnouvass_ss VALUES (2745, 'chambre_de_chute_a_vortex', '99');
    
-- Table: qgep_raepa.qgep_raepa_ouvrass_position_precision

-- DROP TABLE qgep_raepa.qgep_raepa_ouvrass_position_precision;

CREATE TABLE qgep_raepa.qgep_raepa_ouvrass_position_precision
(
    code integer NOT NULL,
    value_fr character varying(50) COLLATE pg_catalog."default",
    raepa_code character varying COLLATE pg_catalog."default",
    CONSTRAINT qgep_raepa_ouvrass_position_precision_pkey PRIMARY KEY (code)
);

INSERT INTO qgep_raepa.qgep_raepa_ouvrass_position_precision VALUES (3243, 'plusque_50cm', '03');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_position_precision VALUES (3241, 'plus_moins_10cm', '01');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_position_precision VALUES (3236, 'plus_moins_3cm', '01');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_position_precision VALUES (3242, 'plus_moins_50cm', '02');
INSERT INTO qgep_raepa.qgep_raepa_ouvrass_position_precision VALUES (5349, 'inconnue', '03');
