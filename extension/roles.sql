/* Viewer */
GRANT USAGE ON SCHEMA qgep_raepa  TO qgep_viewer;
ALTER DEFAULT PRIVILEGES IN SCHEMA qgep_raepa  GRANT SELECT, REFERENCES, TRIGGER ON TABLES TO qgep_viewer;


/* User */
GRANT ALL ON SCHEMA qgep_raepa TO qgep_user;
GRANT ALL ON ALL TABLES IN SCHEMA qgep_raepa TO qgep_user;
ALTER DEFAULT PRIVILEGES IN SCHEMA qgep_raepa GRANT ALL ON TABLES TO qgep_user;

/* Manager */
GRANT ALL ON SCHEMA qgep_raepa TO qgep_manager;
GRANT ALL ON ALL TABLES IN SCHEMA qgep_raepa TO qgep_manager;
ALTER DEFAULT PRIVILEGES IN SCHEMA qgep_raepa GRANT ALL ON TABLES TO qgep_manager;

/* SysAdmin */
GRANT ALL ON SCHEMA qgep_raepa TO qgep_sysadmin;
GRANT ALL ON ALL TABLES IN SCHEMA qgep_raepa TO qgep_sysadmin;
ALTER DEFAULT PRIVILEGES IN SCHEMA qgep_raepa GRANT ALL ON TABLES TO qgep_sysadmin;
